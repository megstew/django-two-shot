from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


# Create your views here.


@login_required
def receipt_view(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt_view": receipts}
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.purchaser = request.user
            form.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {"receipt_form": form}
    return render(request, "receipts/create_receipt.html", context)


@login_required
def category_view(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {"category_view": categories}

    return render(request, "receipts/category.html", context)


@login_required
def account_view(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {"account_view": accounts}
    return render(request, "receipts/account.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            form.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {"category_form": form}
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            form.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {"account_form": form}
    return render(request, "receipts/create_account.html", context)
