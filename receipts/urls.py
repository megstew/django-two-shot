from django.urls import path
from receipts.views import (
    receipt_view,
    create_receipt,
    category_view,
    account_view,
    create_category,
    create_account,
)


urlpatterns = [
    path("accounts/create/", create_account, name="create_account"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/", account_view, name="account_list"),
    path("categories/", category_view, name="category_list"),
    path("create/", create_receipt, name="create_receipt"),
    path("", receipt_view, name="home"),
]
